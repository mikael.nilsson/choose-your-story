# choose your story

Nånstans mellan choose-your-adventure och rollspel. Berättelsen uppdelad inte bara i sidor utan i mindre komponenter där sidor är uppdelade i mindre delar, möjliga att kombinera.

Äventyr kommer behöva ha fasta punkter, men bulken borde kunna sättas ihop i runtime.

Exempeläventyret kommer innehålla ett par rum, ett par NPC:er och ett par monster.

fe byggs i rust, be i golang. Får jag chansen att öva båda :)

Kör lokalt: `func start`

[Update 20230905]: Bygger BE i .Net först, och bygger om i Golang sen, så kan jag koncentrera mig på att öva Rust först och GoLang sen.
