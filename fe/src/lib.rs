mod utils;

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

slint::slint!{
    export component HelloWorld {
        Text {
            text: "hello world";
            color: green;
        }
    }
}

#[wasm_bindgen]
pub fn greet() {
    // alert("Hello, fe!");
    HelloWorld::new().unwrap().run().unwrap();
}
