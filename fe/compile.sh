#!/bin/bash

wasm-pack build
# We need a pkg/package.json with main specified
cp p.json pkg/package.json
cd www
rm -rf node_modules/fe && npm i
